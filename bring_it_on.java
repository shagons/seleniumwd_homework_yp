package BringItOn;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class bring_it_on {
    private WebDriver driver;

    @BeforeMethod (alwaysRun = true)
    public void  browserSetup() {
        driver = new ChromeDriver();
    }

    @Test
    public void theDataIsPasted() throws InterruptedException {

        driver.get("https://pastebin.com/");
        WebElement searchTextInput = driver.findElement(By.id("postform-text"));
        String pasteCode = "git config --global user.name  \"New Sheriff in Town\"\n" +
                "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
                "git push origin master --force";
        searchTextInput.sendKeys(pasteCode);

        WebElement syntaxHighlightingList = driver.findElement(By.id("select2-postform-format-container"));
        syntaxHighlightingList.click();

        WebElement syntaxHighlightingOption = new WebDriverWait(driver, 10).
                until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[@class='select2-results__options select2-results__options--nested']/li[text()='Bash']")));
        syntaxHighlightingOption.click();

        WebElement pasteExpirationList = driver.findElement(By.id("select2-postform-expiration-container"));
        pasteExpirationList.click();

        WebElement pasteExpirationList1 = new WebDriverWait(driver, 10).
                until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[@id='select2-postform-expiration-results']/li[text()='10 Minutes']")));
        pasteExpirationList1.click();

        WebElement searchTitleInput = driver.findElement(By.id("postform-name"));
        searchTitleInput.sendKeys("how to gain dominance among developers");

        WebElement createPasteButton = driver.findElement(By.xpath("//*[@class='form-group form-btn-container']/button[text()='Create New Paste']"));
        createPasteButton.click();

        Thread.sleep(5000);
//        new WebDriverWait(driver, 10).
//                until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class='left']/a[text()='Bash']")));
        Assert.assertTrue(driver.getTitle().contains("how to gain dominance among developers"),"Unexpected title found;");

        WebElement formatingCheck = new WebDriverWait(driver,10).
                until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class='left']/a[text()='Bash']")));
        Assert.assertEquals(formatingCheck.getText(),"Bash","Formating is not verified;");

        WebElement pastedCodeCheck = new WebDriverWait(driver,10).
                until(ExpectedConditions.presenceOfElementLocated(By.xpath("//textarea[@class='textarea']")));
        Assert.assertEquals(pastedCodeCheck.getText(), pasteCode,"Unexpected code was pasted;");


        Thread.sleep(5000);
        driver.quit();
    }

    @AfterMethod (alwaysRun = true)
    public void browserTearDown() {
        driver.quit();
        driver=null;
    }
}
