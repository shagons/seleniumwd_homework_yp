package i_can_win;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.awt.*;

public class i_can_win {
    private WebDriver driver;

    @BeforeMethod (alwaysRun = true)
    public void  browserSetup() {
        driver = new ChromeDriver();
    }

    @Test
    public void theDataIsPasted() throws InterruptedException {

        driver.get("https://pastebin.com/");
        WebElement searchTextInput = driver.findElement(By.id("postform-text"));
        searchTextInput.sendKeys("Hello from WebDriver");
        WebElement pasteExpirationList = driver.findElement(By.id("select2-postform-expiration-container"));
        pasteExpirationList.click();
        WebElement pasteExpirationList1 = new WebDriverWait(driver, 10).
                until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[@id='select2-postform-expiration-results']/li[text()='10 Minutes']")));
        pasteExpirationList1.click();
        WebElement searchTitleInput = driver.findElement(By.id("postform-name"));
        searchTitleInput.sendKeys("helloweb");
        WebElement createPasteButton = driver.findElement(By.xpath("//*[@class='form-group form-btn-container']/button[text()='Create New Paste']"));
        createPasteButton.click();

        Thread.sleep(5000);
        driver.quit();
    }

    @AfterMethod (alwaysRun = true)
    public void browserTearDown() {
        driver.quit();
        driver=null;
    }
}
